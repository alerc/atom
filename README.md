Install:

```
git clone https://gitlab.com/alerc/atom.git ~/.atom
apm install --packages-file ~/.atom/package.list
```

To backup package list:

```
apm list --installed --bare > ~/.atom/package.list
```
